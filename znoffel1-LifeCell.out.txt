*** Life<Cell> 5x9 ***

Generation = 0, Population = 9.
-0------0
0--------
0-----0--
0--0-0---
--------0

Generation = 18, Population = 15.
..**.**..
.1-..---*
-...*..*.
.**.....*
.1...***.

Generation = 36, Population = 14.
......*..
.***.**-.
.......**
.....*.*.
.**..**..

Generation = 54, Population = 0.
.........
.........
.........
.........
.........

Generation = 72, Population = 0.
.........
.........
.........
.........
.........

*** Life<Cell> 8x4 ***

Generation = 0, Population = 19.
00-0
-000
0000
-0-0
0---
--0-
0--0
00-0

Generation = 20, Population = 4.
....
....
....
....
.*..
.*1.
....
.*..

Generation = 40, Population = 6.
....
....
....
....
.**.
*..*
.**.
....

*** Life<Cell> 1x9 ***

Generation = 0, Population = 8.
00000000-

Generation = 12, Population = 0.
....-.-..

Generation = 24, Population = 0.
....-.-..

Generation = 36, Population = 0.
....-.-..

Generation = 48, Population = 0.
....-.-..

*** Life<Cell> 9x6 ***

Generation = 0, Population = 29.
000---
0-000-
--00--
00----
00000-
0---00
---000
-00000
--0--0

Generation = 10, Population = 18.
1..-..
.**.*.
-.**.*
.0-..*
.**--*
.-...*
11.--*
....-.
1.*.-.

Generation = 20, Population = 27.
*.**..
*..*..
**.**.
...**.
...***
.-..**
**.-**
.***.*
..***.

Generation = 30, Population = 5.
.*....
*.*...
**....
......
......
......
...-..
......
......

Generation = 40, Population = 5.
.*....
*.*...
**....
......
......
......
...-..
......
......

Generation = 50, Population = 5.
.*....
*.*...
**....
......
......
......
...-..
......
......

*** Life<Cell> 1x8 ***

Generation = 0, Population = 6.
-0-00000

Generation = 8, Population = 1.
--..---1

Generation = 16, Population = 2.
--..-1-1

Generation = 24, Population = 1.
--..-1--

Generation = 32, Population = 1.
--..---1

Generation = 40, Population = 2.
--..-1-1

*** Life<Cell> 10x9 ***

Generation = 0, Population = 35.
--00--0-0
0--0-0-00
-----000-
0-00-0---
00----0-0
--0------
00-0-0---
------0--
0--0-000-
--0-0--00

Generation = 7, Population = 44.
.****--*.
*-.-1.*11
.-.01.-11
*.**0*0.*
-.1--1-1-
-****1*0-
..--0-.-1
---1-0**-
00--....1
1-*---*1.

*** Life<Cell> 9x4 ***

Generation = 0, Population = 24.
00-0
00-0
-000
00-0
-000
0000
00-0
----
--00

Generation = 19, Population = 7.
--..
-.-.
....
....
...*
..*.
**..
**..
0...

Generation = 38, Population = 4.
--..
-.-.
....
....
....
.**.
.**.
....
....

Generation = 57, Population = 4.
--..
-.-.
....
....
....
.**.
.**.
....
....

*** Life<Cell> 2x3 ***

Generation = 0, Population = 6.
000
000

Generation = 4, Population = 0.
-.-
-.-

Generation = 8, Population = 0.
-.-
-.-

Generation = 12, Population = 0.
-.-
-.-

Generation = 16, Population = 0.
-.-
-.-

Generation = 20, Population = 0.
-.-
-.-

Generation = 24, Population = 0.
-.-
-.-

Generation = 28, Population = 0.
-.-
-.-

Generation = 32, Population = 0.
-.-
-.-

Generation = 36, Population = 0.
-.-
-.-

*** Life<Cell> 10x1 ***

Generation = 0, Population = 8.
-
0
0
0
-
0
0
0
0
0

Generation = 17, Population = 1.
.
.
.
.
.
.
0
-
-
-

Generation = 34, Population = 2.
.
.
.
.
.
.
-
1
-
1

Generation = 51, Population = 1.
.
.
.
.
.
.
-
-
0
-

Generation = 68, Population = 1.
.
.
.
.
.
.
-
-
-
1

Generation = 85, Population = 2.
.
.
.
.
.
.
0
-
0
-

*** Life<Cell> 5x4 ***

Generation = 0, Population = 14.
0-00
0-00
0000
-00-
00--

Generation = 4, Population = 14.
11*-
-0-*
010*
1--0
*-10

Generation = 8, Population = 6.
.**-
---*
-.-.
1*.-
.-*-

Generation = 12, Population = 4.
...-
*--.
..-.
...-
**.1

Generation = 16, Population = 1.
...-
.--.
..-.
...-
...1

Generation = 20, Population = 1.
...-
.--.
..-.
...-
...1

Generation = 24, Population = 1.
...-
.--.
..-.
...-
...1

Generation = 28, Population = 1.
...-
.--.
..-.
...-
...1

Generation = 32, Population = 1.
...-
.--.
..-.
...-
...1

Generation = 36, Population = 1.
...-
.--.
..-.
...-
...1

Generation = 40, Population = 1.
...-
.--.
..-.
...-
...1

Generation = 44, Population = 1.
...-
.--.
..-.
...-
...1

Generation = 48, Population = 1.
...-
.--.
..-.
...-
...1

Generation = 52, Population = 1.
...-
.--.
..-.
...-
...1

Generation = 56, Population = 1.
...-
.--.
..-.
...-
...1

Generation = 60, Population = 1.
...-
.--.
..-.
...-
...1

Generation = 64, Population = 1.
...-
.--.
..-.
...-
...1

Generation = 68, Population = 1.
...-
.--.
..-.
...-
...1

Generation = 72, Population = 1.
...-
.--.
..-.
...-
...1

Generation = 76, Population = 1.
...-
.--.
..-.
...-
...1

Generation = 80, Population = 1.
...-
.--.
..-.
...-
...1

Generation = 84, Population = 1.
...-
.--.
..-.
...-
...1

Generation = 88, Population = 1.
...-
.--.
..-.
...-
...1

*** Life<Cell> 6x4 ***

Generation = 0, Population = 15.
00--
--00
----
0000
-000
0000

Generation = 3, Population = 16.
1*1*
0-.1
*-11
*1*-
-.0-
-**0

Generation = 6, Population = 13.
**1.
-0**
*---
*-.0
1.--
1*.*

Generation = 9, Population = 11.
.***
--.*
.*-1
..*-
*.0-
**..

Generation = 12, Population = 9.
....
--**
..1.
..*-
*.-*
***.

Generation = 15, Population = 11.
.*..
1-*.
*.-*
*..-
*.**
..**

Generation = 18, Population = 7.
*.*.
.**.
*.-.
...0
..*.
....

Generation = 21, Population = 3.
..*.
.**.
..-.
...-
....
....

Generation = 24, Population = 3.
..*.
...*
..*.
...-
....
....

Generation = 27, Population = 0.
....
....
....
...-
....
....

Generation = 30, Population = 0.
....
....
....
...-
....
....

Generation = 33, Population = 0.
....
....
....
...-
....
....

Generation = 36, Population = 0.
....
....
....
...-
....
....

Generation = 39, Population = 0.
....
....
....
...-
....
....

Generation = 42, Population = 0.
....
....
....
...-
....
....

Generation = 45, Population = 0.
....
....
....
...-
....
....

*** Life<Cell> 8x7 ***

Generation = 0, Population = 13.
-000---
0------
-------
-------
---0---
----00-
-0-0--0
-0-0--0

Generation = 2, Population = 20.
0-0-10-
0-1----
-00----
0---00-
---0-10
-10----
-*--00*
-------

Generation = 4, Population = 25.
*1-1.*1
*-1--1-
--0---0
-0-0--0
-100---
---0-10
-.0**-.
-*0---*

Generation = 6, Population = 25.
*.--*.*
.1.--.1
--*--0-
1*0-*1-
11---*1
--*0*.-
-.-..-*
1*01-1.

Generation = 8, Population = 26.
..1-**.
.1.0-.*
1-*1-*-
1.-1..-
11--1..
1-*0**1
-.0*.-.
**-.0-.

Generation = 10, Population = 22.
..1-**.
.1.--*.
.*.*0.1
-.0*..-
**-*.*.
*-.*.**
0*-..-.
..-.-*.

Generation = 12, Population = 24.
..--**.
...-1.*
**..*.1
*.*...-
*.1*...
.-.....
0*1**0*
..0*1*.

Generation = 14, Population = 24.
..1-**.
...--.*
**.*.**
*.**..*
*.-....
*-..***
-.*.*-*
.*0...*

Generation = 16, Population = 21.
..11**.
**.*-.*
*......
*......
*.-...*
*-....*
-....**
.*0*.**

Generation = 18, Population = 21.
**.*.*.
*..*1*.
..*....
.......
..1....
.-.....
*****.*
.*0***.

Generation = 20, Population = 11.
****...
*...1*.
...**..
.......
..-....
.-.....
*......
.*-....

Generation = 22, Population = 11.
*****..
*...1*.
....**.
....*..
..-....
.-.....
.......
..-....

Generation = 24, Population = 9.
*.*.**.
*....*.
...*.*.
....*..
..-....
.-.....
.......
..-....

Generation = 26, Population = 8.
..*.***
..*...*
.....**
.......
..-....
.-.....
.......
..-....

Generation = 28, Population = 5.
...*.*.
...*...
....**.
.......
..-....
.-.....
.......
..-....

Generation = 30, Population = 4.
....*..
...*.*.
....*..
.......
..-....
.-.....
.......
..-....

Generation = 32, Population = 4.
....*..
...*.*.
....*..
.......
..-....
.-.....
.......
..-....

Generation = 34, Population = 4.
....*..
...*.*.
....*..
.......
..-....
.-.....
.......
..-....

Generation = 36, Population = 4.
....*..
...*.*.
....*..
.......
..-....
.-.....
.......
..-....

Generation = 38, Population = 4.
....*..
...*.*.
....*..
.......
..-....
.-.....
.......
..-....

Generation = 40, Population = 4.
....*..
...*.*.
....*..
.......
..-....
.-.....
.......
..-....

Generation = 42, Population = 4.
....*..
...*.*.
....*..
.......
..-....
.-.....
.......
..-....

Generation = 44, Population = 4.
....*..
...*.*.
....*..
.......
..-....
.-.....
.......
..-....

Generation = 46, Population = 4.
....*..
...*.*.
....*..
.......
..-....
.-.....
.......
..-....

Generation = 48, Population = 4.
....*..
...*.*.
....*..
.......
..-....
.-.....
.......
..-....

Generation = 50, Population = 4.
....*..
...*.*.
....*..
.......
..-....
.-.....
.......
..-....

Generation = 52, Population = 4.
....*..
...*.*.
....*..
.......
..-....
.-.....
.......
..-....

Generation = 54, Population = 4.
....*..
...*.*.
....*..
.......
..-....
.-.....
.......
..-....

Generation = 56, Population = 4.
....*..
...*.*.
....*..
.......
..-....
.-.....
.......
..-....

Generation = 58, Population = 4.
....*..
...*.*.
....*..
.......
..-....
.-.....
.......
..-....

Generation = 60, Population = 4.
....*..
...*.*.
....*..
.......
..-....
.-.....
.......
..-....

Generation = 62, Population = 4.
....*..
...*.*.
....*..
.......
..-....
.-.....
.......
..-....

Generation = 64, Population = 4.
....*..
...*.*.
....*..
.......
..-....
.-.....
.......
..-....

Generation = 66, Population = 4.
....*..
...*.*.
....*..
.......
..-....
.-.....
.......
..-....

Generation = 68, Population = 4.
....*..
...*.*.
....*..
.......
..-....
.-.....
.......
..-....

Generation = 70, Population = 4.
....*..
...*.*.
....*..
.......
..-....
.-.....
.......
..-....

*** Life<Cell> 6x1 ***

Generation = 0, Population = 6.
0
0
0
0
0
0

Generation = 14, Population = 0.
.
-
.
.
-
.

Generation = 28, Population = 0.
.
-
.
.
-
.

Generation = 42, Population = 0.
.
-
.
.
-
.

Generation = 56, Population = 0.
.
-
.
.
-
.

*** Life<Cell> 8x6 ***

Generation = 0, Population = 25.
--0---
000--0
0--000
--0-00
---000
000---
--00-0
00-0-0

Generation = 3, Population = 27.
-0.*--
00-00-
*--1**
---1.-
*-0.*0
.10000
1*000.
**-.-.

Generation = 6, Population = 15.
11.*1-
-10--*
.0....
*1-..*
.....-
*--.11
-...1.
..-.-.

Generation = 9, Population = 17.
.-***-
0..1-.
.1.*..
*-0...
*.**.0
.*-.-*
-.....
..-.**

Generation = 12, Population = 18.
.-****
-*.11*
*-*.*.
*--.*.
**....
..0.1*
-.....
..-...

Generation = 15, Population = 18.
.-....
0..1-.
......
*1-...
*.**.*
**-***
1****.
..-...

Generation = 18, Population = 11.
.-....
1*.-1.
......
*.*...
.*....
..0***
-.....
..0...

Generation = 21, Population = 19.
.1....
**.1-.
..*...
..*...
.**.**
.*-.**
-**.**
..1*..

Generation = 24, Population = 14.
.*....
..*1-.
...*..
..*...
.**..*
.**..*
-.....
..1**.

Generation = 27, Population = 13.
..**..
..*--.
....*.
..***.
......
**.*..
*.....
.**...

Generation = 30, Population = 11.
.*..*.
.*.*.*
......
......
*...**
...*..
**....
......

Generation = 33, Population = 0.
......
......
......
......
......
......
......
......

Generation = 36, Population = 0.
......
......
......
......
......
......
......
......

Generation = 39, Population = 0.
......
......
......
......
......
......
......
......

Generation = 42, Population = 0.
......
......
......
......
......
......
......
......

Generation = 45, Population = 0.
......
......
......
......
......
......
......
......

Generation = 48, Population = 0.
......
......
......
......
......
......
......
......

Generation = 51, Population = 0.
......
......
......
......
......
......
......
......

Generation = 54, Population = 0.
......
......
......
......
......
......
......
......

*** Life<Cell> 2x10 ***

Generation = 0, Population = 10.
-00-0--0-0
0-0-000---

Generation = 12, Population = 8.
.....-*-*-
...11**.**

Generation = 24, Population = 2.
.......-.-
.......*.*

Generation = 36, Population = 0.
.......-.-
..........

Generation = 48, Population = 0.
.......-.-
..........

Generation = 60, Population = 0.
.......-.-
..........

Generation = 72, Population = 0.
.......-.-
..........

*** Life<Cell> 4x2 ***

Generation = 0, Population = 6.
-0
00
00
-0

Generation = 7, Population = 4.
0.
1.
1.
0.

Generation = 14, Population = 0.
..
..
..
..

Generation = 21, Population = 0.
..
..
..
..

Generation = 28, Population = 0.
..
..
..
..

*** Life<Cell> 3x2 ***

Generation = 0, Population = 3.
0-
0-
-0

Generation = 4, Population = 3.
1-
*-
-1

Generation = 8, Population = 5.
**
.0
**

Generation = 12, Population = 0.
..
.-
..

Generation = 16, Population = 0.
..
.-
..

Generation = 20, Population = 0.
..
.-
..

Generation = 24, Population = 0.
..
.-
..

Generation = 28, Population = 0.
..
.-
..

Generation = 32, Population = 0.
..
.-
..

Generation = 36, Population = 0.
..
.-
..

Generation = 40, Population = 0.
..
.-
..

Generation = 44, Population = 0.
..
.-
..

*** Life<Cell> 9x10 ***

Generation = 0, Population = 33.
-00-0-0-0-
00--00---0
--------0-
-0-----00-
-0-0-0-00-
--0-0-----
-----00-00
-0-0---000
--0---00--

Generation = 15, Population = 36.
.*.***.---
...0-.*...
*.-...*1..
*.**..*..-
*.*.*.-...
**-*******
***..*1-**
-*...*.*..
...*......

Generation = 30, Population = 11.
........*-
.......*.*
..1***..*.
...***....
..........
..-.......
..........
-.........
..........

Generation = 45, Population = 4.
........**
........**
..........
..........
..........
..........
..........
-.........
..........

*** Life<Cell> 10x5 ***

Generation = 0, Population = 11.
-----
00---
-00--
-0---
-----
-----
-0---
-0---
00-00
-----

Generation = 15, Population = 14.
-.-*1
...**
.....
...**
--.**
..--.
.**-*
*.-..
.....
-*-*-

Generation = 30, Population = 21.
0*.*1
*...*
.*.*.
..*..
.-...
..*..
.***.
**.**
.***.
..*.-

Generation = 45, Population = 0.
-...-
.....
.....
.....
.-...
.....
.....
.....
.....
....-

Generation = 60, Population = 0.
-...-
.....
.....
.....
.-...
.....
.....
.....
.....
....-

*** Life<Cell> 8x7 ***

Generation = 0, Population = 26.
0000-0-
-0--0-0
0--0-0-
-0-----
-0-----
00-0-00
0-00--0
000---0

Generation = 19, Population = 23.
.......
**1***.
..-1.*.
...**..
.1*....
***.**.
...***.
.-.***.

Generation = 38, Population = 0.
.......
.......
.......
.......
.......
.......
.......
.-.....

*** Life<Cell> 9x3 ***

Generation = 0, Population = 13.
00-
0-0
0--
---
000
0--
--0
0-0
-0-
